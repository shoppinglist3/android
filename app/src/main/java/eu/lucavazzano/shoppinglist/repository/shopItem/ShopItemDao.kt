package eu.lucavazzano.shoppinglist.repository.shopItem

import androidx.lifecycle.LiveData
import androidx.room.*
import eu.lucavazzano.shoppinglist.modelViews.SHOP_ITEM_EXPANDED_VIEW_NAME
import eu.lucavazzano.shoppinglist.modelViews.ShopItemExpanded
import eu.lucavazzano.shoppinglist.models.ShopItem

@Suppress("ConvertToStringTemplate")
// ^ string templates are not constant at compile time so using them for @Query doesn't work
@Dao
interface ShopItemDao {
    @Query("SELECT * FROM $SHOP_ITEM_EXPANDED_VIEW_NAME")
    fun getAllExpanded(): LiveData<List<ShopItemExpanded>>

    @Query("SELECT * FROM $SHOP_ITEM_EXPANDED_VIEW_NAME WHERE id = :id")
    fun getExpanded(id: Long): LiveData<ShopItemExpanded>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: ShopItem)

    @Delete
    suspend fun delete(item: ShopItem)

    @Update
    suspend fun update(category: ShopItem)

}
