package eu.lucavazzano.shoppinglist.repository.listItem

import androidx.lifecycle.LiveData
import eu.lucavazzano.shoppinglist.modelViews.ListItemExpanded
import eu.lucavazzano.shoppinglist.models.ListItem
import kotlinx.coroutines.*

class ListItemRepository(private val dao: ListItemDao) {
    val items: LiveData<List<ListItemExpanded>> = dao.getAllExpanded()

    fun getExpanded(id: Long): LiveData<ListItemExpanded> {
        return dao.getExpanded(id)
    }

    suspend fun insert(item: ListItem) = withContext(Dispatchers.IO) {
        dao.insert(item)
    }

    suspend fun delete(item: ListItem) = withContext(Dispatchers.IO) {
        dao.delete(item)
    }

    suspend fun update(item: ListItem) = withContext(Dispatchers.IO) {
        dao.update(item)
    }

}
