package eu.lucavazzano.shoppinglist.repository.shopcategory

import androidx.lifecycle.LiveData
import androidx.room.*
import eu.lucavazzano.shoppinglist.models.SHOP_CATEGORY_TABLE_NAME
import eu.lucavazzano.shoppinglist.models.ShopCategory

@Suppress("ConvertToStringTemplate")
// ^ string templates are not constant at compile time so using them for @Query doesn't work
@Dao
interface ShopCategoryDao {
    @Query("SELECT * FROM $SHOP_CATEGORY_TABLE_NAME")
    fun getAll(): LiveData<List<ShopCategory>>

    @Query("SELECT * FROM $SHOP_CATEGORY_TABLE_NAME WHERE id = :id")
    fun get(id: Long): LiveData<ShopCategory>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: ShopCategory)

    @Update
    suspend fun update(category: ShopCategory)

}
