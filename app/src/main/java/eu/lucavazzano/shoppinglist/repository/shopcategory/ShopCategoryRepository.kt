package eu.lucavazzano.shoppinglist.repository.shopcategory

import androidx.lifecycle.LiveData
import eu.lucavazzano.shoppinglist.models.ShopCategory
import kotlinx.coroutines.*

class ShopCategoryRepository(private val dao: ShopCategoryDao) {
    val items: LiveData<List<ShopCategory>> = dao.getAll()

    fun get(id: Long): LiveData<ShopCategory> {
        return dao.get(id)
    }

    suspend fun insert(category: ShopCategory) = withContext(Dispatchers.IO) {
        dao.insert(category)
    }

    suspend fun update(category: ShopCategory) = withContext(Dispatchers.IO) {
        dao.update(category)
    }

}
