package eu.lucavazzano.shoppinglist.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import eu.lucavazzano.shoppinglist.modelViews.ListItemExpanded
import eu.lucavazzano.shoppinglist.modelViews.ShopItemExpanded
import eu.lucavazzano.shoppinglist.models.ListItem
import eu.lucavazzano.shoppinglist.models.ShopCategory
import eu.lucavazzano.shoppinglist.models.ShopItem
import eu.lucavazzano.shoppinglist.repository.listItem.ListItemDao
import eu.lucavazzano.shoppinglist.repository.shopItem.ShopItemDao
import eu.lucavazzano.shoppinglist.repository.shopcategory.ShopCategoryDao

@Database(
        entities = [ShopItem::class, ShopCategory::class, ListItem::class],
        views = [ShopItemExpanded::class, ListItemExpanded::class],
        version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getShopItemDao(): ShopItemDao
    abstract fun getShopCategoryDao(): ShopCategoryDao
    abstract fun getListItemDao(): ListItemDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE
                ?: synchronized(this) {
                    val instance: AppDatabase = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "app_db"
                    ).build()
                    INSTANCE = instance
                    instance
                }
        }
    }

}
