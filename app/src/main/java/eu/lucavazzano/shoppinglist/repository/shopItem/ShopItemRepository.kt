package eu.lucavazzano.shoppinglist.repository.shopItem

import androidx.lifecycle.LiveData
import eu.lucavazzano.shoppinglist.modelViews.ShopItemExpanded
import eu.lucavazzano.shoppinglist.models.ShopItem
import kotlinx.coroutines.*

class ShopItemRepository(private val dao: ShopItemDao) {
    val items: LiveData<List<ShopItemExpanded>> = dao.getAllExpanded()

    fun getExpanded(id: Long): LiveData<ShopItemExpanded> {
        return dao.getExpanded(id)
    }

    suspend fun insert(item: ShopItem) = withContext(Dispatchers.IO) {
        dao.insert(item)
    }

    suspend fun delete(item: ShopItem) = withContext(Dispatchers.IO) {
        dao.delete(item)
    }

    suspend fun update(item: ShopItem) = withContext(Dispatchers.IO) {
        dao.update(item)
    }


}
