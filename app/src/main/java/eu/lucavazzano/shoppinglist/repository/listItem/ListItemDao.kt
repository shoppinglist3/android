package eu.lucavazzano.shoppinglist.repository.listItem

import androidx.lifecycle.LiveData
import androidx.room.*
import eu.lucavazzano.shoppinglist.modelViews.LIST_ITEM_EXPANDED_VIEW_NAME
import eu.lucavazzano.shoppinglist.modelViews.ListItemExpanded
import eu.lucavazzano.shoppinglist.models.ListItem

@Suppress("ConvertToStringTemplate")
// ^ string templates are not constant at compile time so using them for @Query doesn't work
@Dao
interface ListItemDao {
    @Query("SELECT * FROM $LIST_ITEM_EXPANDED_VIEW_NAME")
    fun getAllExpanded(): LiveData<List<ListItemExpanded>>

    @Query("SELECT * FROM $LIST_ITEM_EXPANDED_VIEW_NAME WHERE id = :id")
    fun getExpanded(id: Long): LiveData<ListItemExpanded>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: ListItem)

    @Delete
    suspend fun delete(item: ListItem)

    @Update
    suspend fun update(category: ListItem)

}
