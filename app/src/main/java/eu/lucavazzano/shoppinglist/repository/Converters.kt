package eu.lucavazzano.shoppinglist.repository

import android.graphics.Color
import androidx.room.TypeConverter
import eu.lucavazzano.shoppinglist.ui.util.data.IconName

class Converters {
    @TypeConverter
    fun saveColor(color: Color): Long {
        return color.pack()
    }

    @TypeConverter
    fun readColor(value: Long): Color {
        return Color.valueOf(value)
    }

    @TypeConverter
    fun saveIconName(iconName: IconName): String {
        return iconName.name
    }

    @TypeConverter
    fun readIconName(value: String): IconName {
        return try {
            IconName.valueOf(value)
        } catch (ex: IllegalArgumentException) {
            IconName.Android
        }
    }
}
