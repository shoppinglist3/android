package eu.lucavazzano.shoppinglist.modelViews

import android.graphics.Color
import androidx.room.DatabaseView
import eu.lucavazzano.shoppinglist.models.SHOP_CATEGORY_TABLE_NAME
import eu.lucavazzano.shoppinglist.models.SHOP_ITEM_TABLE_NAME
import eu.lucavazzano.shoppinglist.ui.util.data.IconName

const val SHOP_ITEM_EXPANDED_VIEW_NAME = "shop_item_expanded_view"

@DatabaseView(
        "SELECT item.id, item.name, item.categoryId, " +
                "  category.name AS categoryName, " +
                "  category.color AS categoryColor, " +
                "  category.iconName AS categoryIconName " +
                "FROM $SHOP_ITEM_TABLE_NAME AS item " +
                "INNER JOIN $SHOP_CATEGORY_TABLE_NAME as category " +
                "ON item.categoryId = category.id",
        viewName = SHOP_ITEM_EXPANDED_VIEW_NAME
)
data class ShopItemExpanded(
        val id: Long,
        val name: String,
        val categoryId: Long,
        val categoryName: String,
        val categoryColor: Color,
        val categoryIconName: IconName
)
