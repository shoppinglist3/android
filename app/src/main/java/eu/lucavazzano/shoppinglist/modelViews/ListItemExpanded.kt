package eu.lucavazzano.shoppinglist.modelViews

import android.graphics.Color
import androidx.room.DatabaseView
import eu.lucavazzano.shoppinglist.models.LIST_ITEM_TABLE_NAME
import eu.lucavazzano.shoppinglist.ui.util.data.IconName

const val LIST_ITEM_EXPANDED_VIEW_NAME = "list_item_expanded_view"

@DatabaseView(
        "SELECT listItem.id, listItem.amount, listItem.note, " +
                "shopItem.name, shopItem.categoryId, " +
                "shopItem.categoryName, shopItem.categoryColor, shopItem.categoryIconName " +
                "FROM $LIST_ITEM_TABLE_NAME AS listItem " +
                "INNER JOIN $SHOP_ITEM_EXPANDED_VIEW_NAME as shopItem " +
                "ON listItem.shopItemId = shopItem.id",
        viewName = LIST_ITEM_EXPANDED_VIEW_NAME
)
data class ListItemExpanded(
        val id: Long,
        val amount: String,
        val note: String,
        val name: String,
        val categoryId: Long,
        val categoryName: String,
        val categoryColor: Color,
        val categoryIconName: IconName
)
