package eu.lucavazzano.shoppinglist.ui.composables.pickers

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.models.ShopCategory
import eu.lucavazzano.shoppinglist.ui.composables.cards.CategoryCard
import eu.lucavazzano.shoppinglist.ui.util.data.listAllIcons
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.*
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import eu.lucavazzano.shoppinglist.ui.util.helpers.toGraphics

@Composable
fun CategoryPicker(
        categories: List<ShopCategory>,
        onSelect: (v: ShopCategory) -> Unit,
        modifier: Modifier = Modifier,
) {
    Box {
        Surface(
                color = MaterialTheme.colors.scrim,
                modifier = modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
        ) {}
        Card(
                modifier = modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                        .padding(edgePaddingSize)
        ) {
            Column(
                    modifier = Modifier.edgePadding()
            ) {
                Text(
                        text = getString(resId = R.string.label_select_category),
                        style = typography.h5
                )
                SectionSpacer()
                LazyColumn {
                    items(items = categories) {
                        CategoryCard(
                                name = it.name,
                                color = it.color.toCompose(),
                                iconName = it.iconName,
                                showEditIcon = false,
                                onClick = { onSelect(it) },
                                modifier = Modifier.padding(bottom = contentPaddingSize)
                        )
                    }
                }
            }
        }
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        Box {
            Text(
                    text = getString(R.string.placeholder_lipsum),
                    style = typography.h4,
                    modifier = Modifier.edgePadding()
            )
            CategoryPicker(
                    categories = listOf(
                            ShopCategory(
                                    "nice category",
                                    listVividColors()[3].toGraphics(),
                                    listAllIcons()[1]),
                            ShopCategory(
                                    "random category",
                                    listVividColors()[7].toGraphics(),
                                    listAllIcons()[4],
                            )
                    ),
                    onSelect = { }
            )
        }
    }
}

@Preview
@Composable
private fun LightPreview() {
    ShoppingListTheme {
        Box {
            Text(
                    text = getString(R.string.placeholder_lipsum),
                    style = typography.h4,
                    modifier = Modifier.edgePadding()
            )
            CategoryPicker(
                    categories = listOf(
                            ShopCategory(
                                    "nice category",
                                    listVividColors()[3].toGraphics(),
                                    listAllIcons()[1]),
                            ShopCategory(
                                    "random category",
                                    listVividColors()[7].toGraphics(),
                                    listAllIcons()[4],
                            )
                    ),
                    onSelect = { }
            )
        }
    }
}
