package eu.lucavazzano.shoppinglist.ui.screens.settings

import android.content.Context
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.*
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.modelViews.ShopItemExpanded
import eu.lucavazzano.shoppinglist.models.ShopCategory
import eu.lucavazzano.shoppinglist.models.ShopItem
import eu.lucavazzano.shoppinglist.repository.AppDatabase
import eu.lucavazzano.shoppinglist.repository.shopItem.ShopItemRepository
import eu.lucavazzano.shoppinglist.repository.shopcategory.ShopCategoryRepository
import eu.lucavazzano.shoppinglist.ui.composables.FullscreenIndeterminateProgressIndicator
import eu.lucavazzano.shoppinglist.ui.composables.cards.CategoryCard
import eu.lucavazzano.shoppinglist.ui.composables.pickers.CategoryPicker
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.listAllIcons
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.SectionSpacer
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.edgePadding
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import kotlinx.coroutines.launch

class ItemEditScreenViewModel(
        selectedIdString: String?,
        namePrefill: String?,
        context: Context
): ViewModel() {
    private val itemRepository: ShopItemRepository
    private val selectedId: Long?
    val initialItem: LiveData<ShopItemExpanded>
    private val categoryRepository: ShopCategoryRepository
    val categories: LiveData<List<ShopCategory>>

    init {
        val db = AppDatabase.getDatabase(context)
        val itemDao = db.getShopItemDao()
        itemRepository = ShopItemRepository(itemDao)
        val categoryDao = db.getShopCategoryDao()
        categoryRepository = ShopCategoryRepository(categoryDao)

        categories = categoryRepository.items

        if (selectedIdString != null) {
            try {
                selectedId = selectedIdString.toLong()
            } catch (_: NumberFormatException) {
                throw IllegalArgumentException("Invalid selected id passed.")
            }
            initialItem = itemRepository.getExpanded(selectedId)

        } else {
            selectedId = null
            initialItem = Transformations.switchMap(categories) { currentCategories ->
                val category = currentCategories.random()
                MutableLiveData(
                        ShopItemExpanded(
                                id = 0,
                                name = namePrefill ?: "",
                                categoryId = category.id,
                                categoryName = category.name,
                                categoryColor = category.color,
                                categoryIconName = category.iconName
                        )
                )
            }
        }
    }

    fun addOrUpdateCategory(name: String, categoryId: Long) {
        viewModelScope.launch {
            if (selectedId != null) {
                itemRepository.update(ShopItem(name, categoryId, id = selectedId))
            } else {
                itemRepository.insert(ShopItem(name, categoryId))
            }
        }
    }
}


@Composable
fun ItemEditScreen(
        categoryId: String?,
        namePrefill: String?,
        navHostController: NavHostController
) {
    ItemEditScreen(
            ItemEditScreenViewModel(categoryId, namePrefill, LocalContext.current),
            navHostController
    )
}

@Composable
fun ItemEditScreen(vm: ItemEditScreenViewModel, navHostController: NavHostController) {
    vm.initialItem.observeAsState().value?.let { selectedItem ->
        ItemEditScreen(
                givenName = selectedItem.name,
                givenCategoryId = selectedItem.categoryId,
                givenCategoryName = selectedItem.categoryName,
                givenCategoryColor = selectedItem.categoryColor.toCompose(),
                givenCategoryIconName = selectedItem.categoryIconName,
                possibleCategoriesState = vm.categories.observeAsState(),
        ) { name, categoryId ->
            vm.addOrUpdateCategory(name, categoryId)
            navHostController.popBackStack()
        }
    } ?: FullscreenIndeterminateProgressIndicator()
}

@Composable
fun ItemEditScreen(
        givenName: String,
        givenCategoryId: Long,
        givenCategoryName: String,
        givenCategoryColor: Color,
        givenCategoryIconName: IconName,
        possibleCategoriesState: State<List<ShopCategory>?>,
        onSave: (name: String, categoryId: Long) -> Unit
) {
    val nameState = remember { mutableStateOf(givenName) }
    val categoryIdState = remember { mutableStateOf(givenCategoryId) }
    val categoryNameState = remember { mutableStateOf(givenCategoryName) }
    val categoryColorState = remember { mutableStateOf(givenCategoryColor) }
    val categoryIconNameState = remember { mutableStateOf(givenCategoryIconName) }

    val name by nameState
    var categoryId by categoryIdState
    var categoryName by categoryNameState
    var categoryColor by categoryColorState
    var categoryIconName by categoryIconNameState

    val possibleCategories by possibleCategoriesState

    var isCategoryPickerOpen by remember { mutableStateOf(false) }

    Box {
        Scaffold(
                content = {
                    BodyContent(
                            nameState = nameState,
                            categoryName = categoryName,
                            categoryColor = categoryColor,
                            categoryIconName = categoryIconName,
                            onCategoryClick = { isCategoryPickerOpen = true },
                    )
                },
                bottomBar = {
                    Button(
                            onClick = { onSave(name, categoryId) },
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .edgePadding()
                    ) {
                        Text(text = getString(R.string.button_save))
                    }
                }
        )

        if (isCategoryPickerOpen) {
            CategoryPicker(
                    categories = possibleCategories ?: listOf(),
                    onSelect = {
                        categoryId = it.id
                        categoryName = it.name
                        categoryColor = it.color.toCompose()
                        categoryIconName = it.iconName
                        isCategoryPickerOpen = false
                    }
            )
        }
    }
}

@Composable
private fun BodyContent(
        nameState: MutableState<String>,
        categoryName: String,
        categoryColor: Color,
        categoryIconName: IconName,
        onCategoryClick: () -> Unit,
) {
    var name by nameState

    val focusRequester = FocusRequester()

    Column(
            modifier = Modifier
                    .fillMaxSize()
                    .edgePadding()
    ) {
        Text(
                text = getString(R.string.title_item_edit),
                style = MaterialTheme.typography.h4,
        )
        SectionSpacer()
        OutlinedTextField(
                textStyle = MaterialTheme.typography.body1.copy(
                        color = MaterialTheme.colors.onBackground
                ),
                label = { Text(text = getString(R.string.label_name)) },
                value = name,
                onValueChange = { name = it },
                singleLine = true,
                modifier = Modifier
                        .fillMaxWidth()
                        .focusModifier()
                        .focusRequester(focusRequester)
        )
        SectionSpacer()
        Text(
                text = getString(R.string.label_select_category),
                modifier = Modifier.contentPadding()
        )
        CategoryCard(
                name = categoryName,
                color = categoryColor,
                iconName = categoryIconName,
                showEditIcon = false,
                onClick = { onCategoryClick() }
        )
    }

    DisposableEffect(Unit) {
        focusRequester.requestFocus()
        onDispose { }
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        ItemEditScreen(
                givenName = "",
                givenCategoryId = 0,
                givenCategoryName = "Fruits & Vegetables",
                givenCategoryColor = listVividColors()[10],
                givenCategoryIconName = listAllIcons().random(),
                possibleCategoriesState = remember { mutableStateOf(listOf()) }
        ) { _, _ -> }
    }
}
