package eu.lucavazzano.shoppinglist.ui.screens.main

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.icons.sharp.DeleteForever
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.modelViews.ListItemExpanded
import eu.lucavazzano.shoppinglist.models.ListItem
import eu.lucavazzano.shoppinglist.repository.AppDatabase
import eu.lucavazzano.shoppinglist.repository.listItem.ListItemRepository
import eu.lucavazzano.shoppinglist.ui.composables.cards.ItemCard
import eu.lucavazzano.shoppinglist.ui.composables.util.NavDrawer
import eu.lucavazzano.shoppinglist.ui.util.data.listAllIcons
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.contentPaddingSize
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import eu.lucavazzano.shoppinglist.ui.util.helpers.toGraphics
import eu.lucavazzano.shoppinglist.ui.util.navigation.Screen
import eu.lucavazzano.shoppinglist.ui.util.navigation.navigate
import kotlinx.coroutines.launch

class ListScreenViewModel(context: Context) : ViewModel() {
    private val repository: ListItemRepository
    var items: LiveData<List<ListItemExpanded>>

    init {
        val dao = AppDatabase.getDatabase(context).getListItemDao()
        repository = ListItemRepository(dao)
        items = repository.items
    }

    fun removeItem(itemId: Long) {
        viewModelScope.launch {
            repository.delete(ListItem("", "", 0, id = itemId))
        }
    }
}


@Composable
fun ListScreen(navController: NavHostController) {
    ListScreen(ListScreenViewModel(LocalContext.current), navController)
}

@Composable
fun ListScreen(vm: ListScreenViewModel, navController: NavHostController) {
    val itemsList by vm.items.observeAsState(initial = emptyList())

    ListScreen(
        items = itemsList,
        onRemoveItem = { vm.removeItem(it.id) },
        onAddItemClick = { navController.navigate(Screen.AddToList) },
        onStartShoppingClick = { navController.navigate(Screen.Shopping) },
        navController = navController,
    )
}

@Composable
fun ListScreen(
    items: List<ListItemExpanded>,
    onAddItemClick: () -> Unit,
    onRemoveItem: (ListItemExpanded) -> Unit,
    onStartShoppingClick: () -> Unit,
    navController: NavHostController?,
) {
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val drawerScope = rememberCoroutineScope()

    NavDrawer(
        drawerState = drawerState,
        drawerScope = drawerScope,
        navController = navController
    ) {
        Scaffold(
            topBar = {
                TopBarContent(
                    onMenuClick = {
                        drawerScope.launch { drawerState.open() }
                    },
                    onAddItemClick = onAddItemClick
                )
            },
            floatingActionButton = {
                StartShoppingContent(
                    onClick = onStartShoppingClick
                )
            }
        ) { innerPadding ->
            ListContent(
                items = items,
                onRemoveItem = onRemoveItem,
                modifier = Modifier
                    .padding(innerPadding)
                    .contentPadding()
            )
        }
    }
}

@Composable
private fun TopBarContent(
    onMenuClick: () -> Unit,
    onAddItemClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Surface(
        color = MaterialTheme.colors.primarySurface,
        elevation = 4.dp,
        modifier = modifier.fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(onClick = { onMenuClick() }) {
                Icon(
                    imageVector = Icons.Default.Menu,
                    contentDescription = getString(R.string.describe_open_navdrawer)
                )
            }
            OutlinedTextField(
                value = "",
                onValueChange = {},
                placeholder = { Text(getString(R.string.label_add_item)) },
                enabled = false,
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable(onClick = { onAddItemClick() })
                    .padding(
                        end = contentPaddingSize,
                        top = contentPaddingSize,
                        bottom = contentPaddingSize
                    )
            )
        }
    }
}

@Composable
private fun ListContent(
    items: List<ListItemExpanded>,
    onRemoveItem: (ListItemExpanded) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(modifier = modifier) {
        items(items = items) {
            ItemCard(
                ribbonColor = it.categoryColor.toCompose(),
                iconName = it.categoryIconName,
                text = buildString {
                    if (it.amount.isNotEmpty()) {
                        append("${it.amount} ")
                    }
                    append(it.name)
                    if (it.note.isNotEmpty()) {
                        append(" (${it.note})")
                    }
                },
                onClick = { onRemoveItem(it) },
                trailingIcon = Icons.Sharp.DeleteForever,
                modifier = Modifier.padding(bottom = contentPaddingSize)
            )
        }
    }
}

@Composable
private fun StartShoppingContent(
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    FloatingActionButton(
        contentColor = MaterialTheme.colors.onPrimary,
        onClick = onClick,
        modifier = modifier
    ) {
        Icon(
            imageVector = Icons.Filled.ShoppingCart,
            contentDescription = getString(R.string.describe_start_shopping),
        )
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    val items = listOf<ListItemExpanded>(
        ListItemExpanded(
            0, "17", "", "Limes",
            0, "Fruits", listVividColors()[10].toGraphics(), listAllIcons()[4]
        ),
        ListItemExpanded(
            0, "420g", "", "Chocolate",
            0, "Snacks", listVividColors()[2].toGraphics(), listAllIcons()[5]
        ),
    )

    ShoppingListTheme(darkTheme = true) {
        ListScreen(items, {}, {}, {}, null)
    }
}

@Preview
@Composable
private fun LightPreview() {
    val items = listOf<ListItemExpanded>(
        ListItemExpanded(
            0, "17", "", "Limes",
            0, "Fruits", listVividColors()[10].toGraphics(), listAllIcons()[4]
        ),
        ListItemExpanded(
            0, "420g", "", "Chocolate",
            0, "Snacks", listVividColors()[2].toGraphics(), listAllIcons()[5]
        ),
    )

    ShoppingListTheme {
        ListScreen(items, {}, {}, {}, null)
    }
}
