package eu.lucavazzano.shoppinglist.ui.screens.intro

import android.content.Context
import android.content.SharedPreferences
import eu.lucavazzano.shoppinglist.ui.util.navigation.Screen

enum class IntroStep(val value: String) {
    GREETING("GREETING"),
    DONE("DONE"),
}

internal fun getNextScreen(current: IntroStep): Screen {
    return when (current) {
        IntroStep.GREETING -> Screen.IntroDone
        IntroStep.DONE -> Screen.List
    }
}

internal fun setSavedStep(context: Context, step: IntroStep) {
    val pref = getSharedPreferences(context)
    with(pref.edit()) {
        putString(INTRO_STEP_KEY, step.value)
        apply()
    }
}

internal fun getSavedStep(context: Context): IntroStep {
    val pref = getSharedPreferences(context)
    val step = pref.getString(INTRO_STEP_KEY, IntroStep.GREETING.value)!!
    return IntroStep.valueOf(step)
}

fun getStartScreenForSavedIntroStep(context: Context): Screen {
    return when (getSavedStep(context)) {
        IntroStep.DONE -> getNextScreen(IntroStep.DONE)
        else -> Screen.IntroGreeting // go to greeting first, than continue to next step
    }
}


private fun getSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences("intro_state", Context.MODE_PRIVATE)
}

private const val INTRO_STEP_KEY = "step"
