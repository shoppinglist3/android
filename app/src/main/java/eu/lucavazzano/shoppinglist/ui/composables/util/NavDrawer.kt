package eu.lucavazzano.shoppinglist.ui.composables.util

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Category
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Store
import androidx.compose.material.icons.filled.ViewQuilt
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.design.*
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.navigation.Screen
import eu.lucavazzano.shoppinglist.ui.util.navigation.navigate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun NavDrawer(
        drawerState: DrawerState,
        drawerScope: CoroutineScope,
        navController: NavHostController?,
        content: @Composable () -> Unit
) {
    ModalDrawer(
            drawerState = drawerState,
            scrimColor = MaterialTheme.colors.scrim,
            drawerContent = {
                Column(
                        modifier = Modifier
                                .edgePadding()
                                .padding(start = edgePaddingSize + contentPaddingSize)
                ) {
                    TopBarContent(
                            drawerState = drawerState,
                            drawerScope = drawerScope
                    )
                    ContentSpacer()
                    ListContent(navController)
                    Spacer(modifier = Modifier.fillMaxHeight())
                    RoboImage(modifier = Modifier.height(200.dp))
                }
        },
            content = {
                content()
            }
    )
}

@Composable
private fun TopBarContent(drawerState: DrawerState, drawerScope: CoroutineScope) {
    Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
    ) {
        Text(
                text = getString(R.string.label_settings),
                style = typography.h5,
        )
        IconButton(
                onClick = {
                    drawerScope.launch { drawerState.close() }
                },
                modifier = Modifier.alpha(0.7f)
        ) {
            Icon(
                    Icons.Default.Close,
                    contentDescription = getString(R.string.describe_close_navdrawer)
            )
        }
    }
}

@Composable
private fun ListContent(navController: NavHostController?) {
    Column {
        NavButton(
                icon = Icons.Default.Category,
                text = getString(R.string.label_list_item),
                target = Screen.ItemsList,
                navController = navController
        )
        NavButton(
                icon = Icons.Default.ViewQuilt,
                text = getString(R.string.label_list_category),
                target = Screen.CategoriesList,
                navController = navController
        )
        NavButton(
                icon = Icons.Default.Store,
                text = getString(R.string.label_list_store),
                target = Screen.StoresList,
                navController = navController
        )
    }
}

@Composable
private fun NavButton(
    icon: ImageVector, text: String, target: Screen, navController: NavHostController?
) {
    TextButton(
        onClick = { navController?.navigate(target) },
        modifier = Modifier.contentPadding()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Icon(
                    imageVector = icon,
                    contentDescription = null // purely decorative, text given later
            )
            Spacer(modifier = Modifier.width(contentPaddingSize))
            Text(text = text)
        }
    }
}

@Composable
private fun RoboImage(modifier: Modifier = Modifier) {
    val image: Painter = painterResource(id = R.drawable.ic_sneaky_robot)
    Image(
            painter = image,
            contentDescription = null, // purely decorative
            modifier = modifier
    )
}


@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun DefaultPreview() {
    val drawerState = rememberDrawerState(DrawerValue.Open)
    val scope = rememberCoroutineScope()

    ShoppingListTheme(darkTheme = true) {
        NavDrawer(drawerState, scope, null) { }
    }
}

@Preview
@Composable
private fun LightPreview() {
    val drawerState = rememberDrawerState(DrawerValue.Open)
    val scope = rememberCoroutineScope()

    ShoppingListTheme {
        NavDrawer(drawerState, scope, null) { }
    }
}
