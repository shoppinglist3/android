package eu.lucavazzano.shoppinglist.ui.composables.pickers

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyGridScope
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.design.*
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@ExperimentalFoundationApi
@Composable
fun Picker(
        title: String,
        minSize: Dp,
        modifier: Modifier = Modifier,
        content: LazyGridScope.() -> Unit
) {
    Box {
        Surface(
                color = MaterialTheme.colors.scrim,
                modifier = modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
        ) {}
        Card(
                modifier = modifier
                        .fillMaxWidth()
                        .fillMaxHeight()
                        .padding(edgePaddingSize * 4)
        ) {
            Column(
                    modifier = Modifier.edgePadding()
            ) {
                Text(
                        text = title,
                        style = typography.h5
                )
                SectionSpacer()
                LazyVerticalGrid(
                        cells = GridCells.Adaptive(minSize = minSize),
                        content = content
                )
            }
        }
    }
}


@ExperimentalFoundationApi
@Preview
@Composable
private fun DefaultPreview() {
    val elements = listOf(
            IntArray(170) { it + 1 }
    )

    ShoppingListTheme(darkTheme = true) {
        Box {
            Text(
                    text = getString(R.string.placeholder_lipsum),
                    style = typography.h4,
                    modifier = Modifier.edgePadding()
            )
            Picker(title = getString(R.string.placeholder), 24.dp) {
                items(items = elements) {
                    Text(text = String.format("%03d", it))
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Preview
@Composable
private fun LightPreview() {
    val elements = listOf(
            IntArray(170) { it + 1 }
    )

    ShoppingListTheme {
        Box {
            Text(
                    text = getString(R.string.placeholder_lipsum),
                    style = typography.h4,
                    modifier = Modifier.edgePadding()
            )
            Picker(title = getString(R.string.placeholder), 24.dp) {
                items(items = elements) {
                    Text(text = String.format("%03d", it))
                }
            }
        }
    }
}
