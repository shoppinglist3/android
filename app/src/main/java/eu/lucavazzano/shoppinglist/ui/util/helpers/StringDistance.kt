package eu.lucavazzano.shoppinglist.ui.util.helpers

import java.util.*
import kotlin.math.min

/**
 * Check if string b is included in string a.
 */
fun checkStringContained(
        aInput: String,
        bInput: String
): Boolean {
    val a = aInput.toLowerCase(Locale.ROOT)
    val b = bInput.toLowerCase(Locale.ROOT)

    return a.contains(b)
}

/**
 * Computes the distance between string a and string b, i.e. how many changes are necessary to get
 *  from string a to string b.
 */
fun computeStringDistance(
        aInput: String,
        bInput: String,
        deleteCost: Float = 1.0f,
        insertCost: Float = 1.0f,
        substituteCost: Float = 1.0f,
        transposeCost: Float = 1.0f
): Float {
    /*
     * based on https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance
     */

    val a = aInput.toLowerCase(Locale.ROOT)
    val b = bInput.toLowerCase(Locale.ROOT)

    val d = Array(a.length + 1) { FloatArray(b.length + 1) { 0.0f } }
    for (i in 0..a.length) {
        d[i][0] = i.toFloat()
    }
    for (j in 0..b.length) {
        d[0][j] = j.toFloat()
    }

    for (i in 1..a.length) {
        for (j in 1..b.length) {
            val cost = when {
                a[i - 1] == b[j - 1] -> 0.0f
                else -> substituteCost
            }

            d[i][j] = min(
                    min(
                            // deletion:
                            d[i - 1][j] + deleteCost,
                            // insertion:
                            d[i][j - 1] + insertCost
                    ),
                    // substitution:
                    d[i - 1][j - 1] + cost
            )

            // transposition:
            if (i > 2 && j > 2 && a[i - 1] == b[j - 2] && a[i - 2] == b[j - 1]) {
                d[i][j] = min(
                        d[i][j],
                        d[i - 2][j - 2] + transposeCost
                )
            }
        }
    }

    return d[a.length][b.length]
}
