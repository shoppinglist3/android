package eu.lucavazzano.shoppinglist.ui.screens.settings

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@Composable
fun StoresListScreen() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
                text = getString(R.string.placeholder),
                style = MaterialTheme.typography.h3,
        )
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        StoresListScreen()
    }
}
