package eu.lucavazzano.shoppinglist.ui.screens.settings

import android.content.Context
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.models.ShopCategory
import eu.lucavazzano.shoppinglist.repository.AppDatabase
import eu.lucavazzano.shoppinglist.repository.shopcategory.ShopCategoryRepository
import eu.lucavazzano.shoppinglist.ui.composables.FullscreenIndeterminateProgressIndicator
import eu.lucavazzano.shoppinglist.ui.composables.pickers.ColorPicker
import eu.lucavazzano.shoppinglist.ui.composables.pickers.IconPicker
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.listAllIcons
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.data.resolveIconName
import eu.lucavazzano.shoppinglist.ui.util.design.SectionSpacer
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPaddingSize
import eu.lucavazzano.shoppinglist.ui.util.design.edgePadding
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import eu.lucavazzano.shoppinglist.ui.util.helpers.toGraphics
import kotlinx.coroutines.launch

class CategoryEditScreenViewModel(
        selectedIdString: String?,
        context: Context
): ViewModel() {
    private val repository: ShopCategoryRepository
    private val selectedId: Long?
    val initialCategory: LiveData<ShopCategory>

    init {
        val dao = AppDatabase.getDatabase(context).getShopCategoryDao()
        repository = ShopCategoryRepository(dao)

        if (selectedIdString != null) {
            try {
                selectedId = selectedIdString.toLong()
            } catch (_: NumberFormatException) {
                throw IllegalArgumentException("Invalid selected id passed.")
            }
            initialCategory = repository.get(selectedId)

        } else {
            selectedId = null
            initialCategory = MutableLiveData(
                    ShopCategory("", listVividColors().random().toGraphics(), listAllIcons().random())
            )
        }
    }

    fun addOrUpdateCategory(name: String, color: android.graphics.Color, iconName: IconName) {
        viewModelScope.launch {
            if (selectedId != null) {
                repository.update(ShopCategory(name, color, iconName, selectedId))
            } else {
                repository.insert(ShopCategory(name, color, iconName))
            }
        }
    }
}


@ExperimentalFoundationApi
@Composable
fun CategoryEditScreen(categoryId: String?, navHostController: NavHostController) {
    CategoryEditScreen(
            CategoryEditScreenViewModel(categoryId, LocalContext.current), navHostController
    )
}

@ExperimentalFoundationApi
@Composable
fun CategoryEditScreen(vm: CategoryEditScreenViewModel, navHostController: NavHostController) {
    vm.initialCategory.observeAsState().value?.let { selectedCategory ->
        CategoryEditScreen(
                givenName = selectedCategory.name,
                givenColor = selectedCategory.color.toCompose(),
                givenIconName = selectedCategory.iconName,
                onSave = { name, color, iconName ->
                    vm.addOrUpdateCategory(name, color, iconName)
                    navHostController.popBackStack()
                },
        )
    } ?: FullscreenIndeterminateProgressIndicator()
}

@ExperimentalFoundationApi
@Composable
fun CategoryEditScreen(
        givenName: String, givenColor: Color, givenIconName: IconName,
        onSave: (name: String, Color: android.graphics.Color, iconName: IconName) -> Unit
) {
    val nameState = remember { mutableStateOf(givenName) }
    val colorState = remember { mutableStateOf(givenColor) }
    val iconNameState = remember { mutableStateOf(givenIconName) }

    val name by nameState
    var color by colorState
    var iconName by iconNameState

    var isColorPickerOpen by remember { mutableStateOf(false) }
    var isIconPickerOpen by remember { mutableStateOf(false) }

    Box {
        Scaffold(
                content = {
                    BodyContent(
                            nameState = nameState,
                            colorState = colorState,
                            iconNameState = iconNameState,
                            onColorClick = { isColorPickerOpen = true },
                            onIconClick = { isIconPickerOpen = true },
                    )
                },
                bottomBar = {
                    Button(
                            onClick = { onSave(name, color.toGraphics(), iconName) },
                            modifier = Modifier
                                    .fillMaxWidth()
                                    .edgePadding()
                    ) {
                        Text(text = getString(R.string.button_save))
                    }
                }
        )

        if (isColorPickerOpen) {
            ColorPicker(onSelect = {
                color = it
                isColorPickerOpen = false
            })
        } else if (isIconPickerOpen) {
            IconPicker(onSelect = {
                iconName = it
                isIconPickerOpen = false
            })
        }
    }
}

@Composable
private fun BodyContent(
        nameState: MutableState<String>,
        colorState: MutableState<Color>,
        iconNameState: MutableState<IconName>,
        onColorClick: () -> Unit,
        onIconClick: () -> Unit
) {
    var name by nameState
    val color by colorState
    val iconName by iconNameState

    val focusRequester = FocusRequester()

    Column(
            modifier = Modifier
                    .fillMaxSize()
                    .edgePadding()
    ) {
        Text(
                text = getString(R.string.title_category_edit),
                style = MaterialTheme.typography.h4,
        )
        SectionSpacer()
        OutlinedTextField(
                label = { Text(text = getString(R.string.label_name)) },
                value = name,
                onValueChange = { name = it },
                singleLine = true,
                modifier = Modifier
                        .fillMaxWidth()
                        .focusModifier()
                        .focusRequester(focusRequester)
        )
        SectionSpacer()
        Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                        .fillMaxWidth()
                        .clickable(onClick = onColorClick)
                        .padding(vertical = contentPaddingSize)
        ) {
            Text(
                    text = getString(R.string.label_select_color),
            )
            Surface(
                    shape = RoundedCornerShape(25),
                    color = color,
                    elevation = 2.dp,
                    modifier = Modifier.size(32.dp)
            ) {}
        }
        SectionSpacer()
        Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                        .fillMaxWidth()
                        .clickable(onClick = onIconClick)
                        .padding(vertical = contentPaddingSize)
        ) {
            Text(
                    text = getString(R.string.label_select_icon),
            )
            Icon(
                    imageVector = resolveIconName(iconName),
                    contentDescription = null // purely decorative
            )
        }
    }

    DisposableEffect(Unit) {
        focusRequester.requestFocus()
        onDispose { }
    }
}


@ExperimentalFoundationApi
@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        CategoryEditScreen(
                givenName = "",
                givenColor = listVividColors().random(),
                givenIconName = listAllIcons().random(),
                onSave = { _, _, _ -> }
        )
    }
}
