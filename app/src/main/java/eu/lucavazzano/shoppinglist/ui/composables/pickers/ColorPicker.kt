package eu.lucavazzano.shoppinglist.ui.composables.pickers

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.IconButton
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.contentPaddingSize
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@ExperimentalFoundationApi
@Composable
fun ColorPicker(onSelect: (v: Color) -> Unit, modifier: Modifier = Modifier) {
    val colorSize = 40.dp
    val cellSize = colorSize + contentPaddingSize

    Picker(
            title = getString(R.string.label_select_color),
            minSize = cellSize,
            modifier = modifier
    ) {
        items(items = listVividColors()) {
            IconButton(
                    onClick = { onSelect(it) },
                    modifier = Modifier.contentPadding()
            ) {
                Surface(
                        shape = RoundedCornerShape(25),
                        color = it,
                        elevation = 2.dp,
                        modifier = Modifier.requiredSize(colorSize)
                ) {}
            }
        }
    }
}


@ExperimentalFoundationApi
@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        ColorPicker({})
    }
}
