package eu.lucavazzano.shoppinglist.ui.screens.intro

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@Composable
fun IntroDoneScreen(navController: NavHostController?) {
    setSavedStep(LocalContext.current, IntroStep.DONE)

    Scaffold(
        bottomBar = {
            IntroButtonBar(
                currentIntroStep = IntroStep.DONE,
                navController = navController,
            )
        }
    ) {
        Content()
    }
}

@Composable
private fun Content() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(text = getString(R.string.title_done))
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        IntroDoneScreen(null)
    }
}
