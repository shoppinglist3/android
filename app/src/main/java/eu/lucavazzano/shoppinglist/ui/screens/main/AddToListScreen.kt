package eu.lucavazzano.shoppinglist.ui.screens.main

import android.content.Context
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Add
import androidx.compose.material.icons.sharp.Clear
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusModifier
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.modelViews.ShopItemExpanded
import eu.lucavazzano.shoppinglist.models.ListItem
import eu.lucavazzano.shoppinglist.repository.AppDatabase
import eu.lucavazzano.shoppinglist.repository.listItem.ListItemRepository
import eu.lucavazzano.shoppinglist.repository.shopItem.ShopItemRepository
import eu.lucavazzano.shoppinglist.ui.composables.FullscreenIndeterminateProgressIndicator
import eu.lucavazzano.shoppinglist.ui.composables.cards.ItemCard
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.*
import eu.lucavazzano.shoppinglist.ui.util.helpers.computeStringDistance
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import eu.lucavazzano.shoppinglist.ui.util.helpers.toGraphics
import eu.lucavazzano.shoppinglist.ui.util.navigation.Screen
import eu.lucavazzano.shoppinglist.ui.util.navigation.navigate
import kotlinx.coroutines.launch

class AddToListScreenViewModel(context: Context) : ViewModel() {
    private val shopItemRepository: ShopItemRepository
    var items: LiveData<List<ShopItemExpanded>>
    private val listItemRepository: ListItemRepository

    init {
        val db = AppDatabase.getDatabase(context)

        val shopItemDao = db.getShopItemDao()
        shopItemRepository = ShopItemRepository(shopItemDao)
        items = shopItemRepository.items

        val listItemDao = db.getListItemDao()
        listItemRepository = ListItemRepository(listItemDao)
    }

    fun addListItem(item: ListItem) {
        viewModelScope.launch {
            listItemRepository.insert(item)
        }
    }
}


@Composable
fun AddToListScreen(navController: NavHostController) {
    AddToListScreen(AddToListScreenViewModel(LocalContext.current), navController)
}

@Composable
fun AddToListScreen(vm: AddToListScreenViewModel, navController: NavHostController) {
    val itemsList by vm.items.observeAsState(initial = emptyList())

    if (itemsList.isNotEmpty()) {
        AddToListScreen(
            items = itemsList,
            onAddClick = { item, amount ->
                vm.addListItem(
                    ListItem(
                        amount = amount,
                        note = "",
                        shopItemId = item
                    )
                )
            },
            onCreateClick = { navController.navigate(Screen.ItemCreate, it) },
        )
    } else {
        FullscreenIndeterminateProgressIndicator()
    }
}

@Composable
fun AddToListScreen(
    items: List<ShopItemExpanded>,
    onAddClick: (shopItemId: Long, amount: String) -> Unit,
    onCreateClick: (String) -> Unit,
) {
    val textInputState = remember { mutableStateOf("") }
    val textInput by textInputState

    var amountText by remember { mutableStateOf("") }
    var itemText by remember { mutableStateOf("") }

    var filteredItems by remember { mutableStateOf(listOf<ShopItemExpanded>()) }

    val splitText = splitInput(textInput)

    amountText = splitText.first
    itemText = splitText.second

    filteredItems = when {
        itemText.isBlank() -> items
        else -> items
            .map {
                it to computeStringDistance(
                    itemText, it.name,
                    deleteCost = 5.0f,
                    insertCost = 1.0f,
                    substituteCost = 5.0f,
                    transposeCost = 0.5f
                )
            }
            .sortedBy { (_, strDistance) -> strDistance }
            .filter { (_, strDistance) -> strDistance < 50 }
            .map { (item, _) -> item }
    }

    Scaffold(
        topBar = {
            TopBarContent(
                textState = textInputState
            )
        },
        content = { innerPadding ->
            ListContent(
                items = filteredItems,
                amount = amountText,
                onAddClick = { onAddClick(it.id, amountText) },
                modifier = Modifier.padding(innerPadding)
            )
        },
        bottomBar = {
            BottomBarContent(
                inputText = itemText,
                showCreateButton = itemText.isNotBlank(),
                onCreateClick = { onCreateClick(itemText) }
            )
        },
    )
}

@Composable
private fun TopBarContent(
    textState: MutableState<String>,
    modifier: Modifier = Modifier
) {
    var textValue by textState

    val focusRequester = FocusRequester()

    Surface(
        color = MaterialTheme.colors.primarySurface,
        elevation = 4.dp,
        modifier = modifier.fillMaxWidth()
    ) {
        OutlinedTextField(
            textStyle = MaterialTheme.typography.body1.copy(
                color = MaterialTheme.colors.onPrimarySurface
            ),
            value = textValue,
            onValueChange = { value ->
                textValue = value
            },
            placeholder = { Text(getString(R.string.label_add_item)) },
            singleLine = true,
            trailingIcon = {
                if (textValue.isNotBlank()) {
                    IconButton(onClick = { textValue = "" }) {
                        Icon(
                            imageVector = Icons.Sharp.Clear,
                            contentDescription = getString(R.string.describe_clear_input)
                        )
                    }
                }
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    all = contentPaddingSize
                )
                .focusModifier()
                .focusRequester(focusRequester)
        )
    }

    DisposableEffect(Unit) {
        focusRequester.requestFocus()
        onDispose { }
    }
}

@Composable
private fun ListContent(
    items: List<ShopItemExpanded>,
    amount: String,
    onAddClick: (ShopItemExpanded) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(modifier = modifier) {
        items(items = items) {
            ItemCard(
                ribbonColor = it.categoryColor.toCompose(),
                iconName = it.categoryIconName,
                text = buildString {
                    if (amount.isNotBlank()) {
                        append("$amount ")
                    }
                    append(it.name)
                },
                trailingIcon = Icons.Sharp.Add,
                onClick = { onAddClick(it) },
                modifier = Modifier.contentPadding()
            )
        }
    }
}

@Composable
private fun BottomBarContent(
    inputText: String,
    showCreateButton: Boolean,
    onCreateClick: () -> Unit
) {
    if (showCreateButton) {
        Button(
            onClick = onCreateClick,
            modifier = Modifier
                .edgePadding()
                .fillMaxWidth()
        ) {
            Text(text = getString(R.string.button_define_item, inputText))
        }
    }
}


fun splitInput(inputText: String): Pair<String, String> {
    fun joinWithSpace(elements: List<String>) = elements.joinToString(separator = " ")

    val (amount, item) = inputText.split(" ")
        .filter { it.isNotBlank() }
        .partition {
            it.matches(
                Regex(
                    "(^[0-9.].*)|g|kg|ml|l|pkg",
                    RegexOption.IGNORE_CASE
                )
            )
        }

    return Pair(joinWithSpace(amount), joinWithSpace(item))
}


@Preview
@Composable
private fun DefaultPreview() {
    val items = listOf(
        ShopItemExpanded(
            0, "Mini Carrots",
            0, "Vegetables", listVividColors()[16].toGraphics(), IconName.Art
        ),
        ShopItemExpanded(
            0, "High Quality Rum",
            0, "Drinks", listVividColors()[8].toGraphics(), IconName.Android
        ),
        ShopItemExpanded(
            0, "Frozen Asia Vegetables",
            0, "Frozen Food", listVividColors()[5].toGraphics(), IconName.Frozen
        )
    )

    ShoppingListTheme(darkTheme = true) {
        AddToListScreen(items, { _, _ -> }, { })
    }
}

@Preview
@Composable
private fun LightPreview() {
    val items = listOf(
        ShopItemExpanded(
            0, "Mini Carrots",
            0, "Vegetables", listVividColors()[16].toGraphics(), IconName.Art
        ),
        ShopItemExpanded(
            0, "High Quality Rum",
            0, "Drinks", listVividColors()[8].toGraphics(), IconName.Android
        ),
        ShopItemExpanded(
            0, "Frozen Asia Vegetables",
            0, "Frozen Food", listVividColors()[5].toGraphics(), IconName.Frozen
        )
    )

    ShoppingListTheme {
        AddToListScreen(items, { _, _ -> }, { })
    }
}
