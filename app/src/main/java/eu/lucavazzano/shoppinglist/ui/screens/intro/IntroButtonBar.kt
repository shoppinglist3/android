package eu.lucavazzano.shoppinglist.ui.screens.intro

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.contentPaddingSize
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.navigation.navigate

@Composable
fun IntroButtonBar(
    currentIntroStep: IntroStep,
    navController: NavHostController?,
    showBack: Boolean = true,
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(contentPaddingSize),
        modifier = Modifier
                .contentPadding()
                .fillMaxWidth()
    ) {
        if (showBack) {
            TextButton(
                onClick = { navController?.popBackStack() },
            ) {
                Icon(
                        imageVector = Icons.Default.ArrowBack,
                        contentDescription = null // decorative, described by following text
                )
                Text(getString(R.string.button_back))
            }
        }
        Button(
            onClick = {
                navController?.navigate(
                    getNextScreen(currentIntroStep)
                )
            },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(getString(R.string.button_next))
            Icon(
                    imageVector = Icons.Default.ArrowForward,
                    contentDescription = null // decorative, described by following text
            )
        }
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        IntroButtonBar(IntroStep.DONE, null)
    }
}

@Preview
@Composable
private fun LightPreview() {
    ShoppingListTheme {
        IntroButtonBar(IntroStep.DONE, null)
    }
}

@Preview
@Composable
private fun NoBackPreview() {
    ShoppingListTheme(darkTheme = true) {
        IntroButtonBar(IntroStep.DONE, null, showBack = false)
    }
}
