package eu.lucavazzano.shoppinglist.ui.util.design

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorPalette = darkColors(
    primary = Color(0xFF329333),
    primaryVariant = Color(0xFF67c460),
    secondary = Color(0xFF00641e),
)

private val LightColorPalette = lightColors(
    primary = Color(0xFF00641e),
    primaryVariant = Color(0xFF67c460),
    secondary = Color(0xFF329933),
)


val Colors.onPrimarySurface: Color
    get() =
        if (isLight) onPrimary
        else onSurface

val Colors.activeOnPrimarySurface: Color
    get() =
        if (isLight) primaryVariant
        else primary

val Colors.scrim: Color
    get() =
        if (isLight) onSurface.copy(alpha = 0.3f)
        else background.copy(alpha = 0.7f)


@Composable
fun ShoppingListTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = typography,
        shapes = shapes,
    ) {
        Surface(
            color = MaterialTheme.colors.background,
            content = content,
        )
    }
}
