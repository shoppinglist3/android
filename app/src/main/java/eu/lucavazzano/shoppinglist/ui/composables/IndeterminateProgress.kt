package eu.lucavazzano.shoppinglist.ui.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.edgePadding

@Composable
fun FullscreenIndeterminateProgressIndicator() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .edgePadding()
    ) {
        CircularProgressIndicator()
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        FullscreenIndeterminateProgressIndicator()
    }
}

@Preview
@Composable
private fun LightPreview() {
    ShoppingListTheme {
        FullscreenIndeterminateProgressIndicator()
    }
}
