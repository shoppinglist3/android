package eu.lucavazzano.shoppinglist.ui.util.design

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

// Padding:

val contentPaddingSize = 8.dp

fun Modifier.contentPadding() = this.then(
    Modifier.padding(contentPaddingSize)
)

@Composable
fun ContentSpacer() {
    Spacer(modifier = Modifier.height(contentPaddingSize))
}


val edgePaddingSize = 16.dp

fun Modifier.edgePadding() = this.then(
    Modifier.padding(edgePaddingSize)
)

val imgEdgePaddingSize = 32.dp

fun Modifier.imgEdgePadding() = this.then(
    Modifier.padding(imgEdgePaddingSize)
)


val sectionPaddingSize = 24.dp

@Composable
fun SectionSpacer() {
    Spacer(modifier = Modifier.height(sectionPaddingSize))
}


// big Illustrations:

val imgHeight = 256.dp

fun Modifier.imgHeight() = this.then(
    Modifier.height(imgHeight)
)
