package eu.lucavazzano.shoppinglist.ui.util.navigation

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.compose.*
import androidx.navigation.navArgument
import eu.lucavazzano.shoppinglist.ui.screens.intro.IntroDoneScreen
import eu.lucavazzano.shoppinglist.ui.screens.intro.IntroGreetingScreen
import eu.lucavazzano.shoppinglist.ui.screens.main.AddToListScreen
import eu.lucavazzano.shoppinglist.ui.screens.main.ListScreen
import eu.lucavazzano.shoppinglist.ui.screens.main.ShoppingScreen
import eu.lucavazzano.shoppinglist.ui.screens.settings.*

// Nav Graph:

sealed class Screen(
        val routeDefinition: String,
        val routeFormat: String? = null,
        val routeDefault: String? = null,
) {
    object IntroGreeting : Screen(routeDefinition = "IntroGreeting")
    object IntroDone : Screen(routeDefinition = "IntroDone")

    object List : Screen(routeDefinition = "list")
    object AddToList : Screen(routeDefinition = "AddToList")

    object Shopping : Screen(routeDefinition = "Shopping")

    object CategoriesList : Screen(routeDefinition = "CategoriesList")
    object CategoryCreate : Screen(routeDefinition = "CategoryCreate")
    object CategoryEdit : Screen(routeDefinition = "CategoryEdit?categoryId={categoryId}",
            routeFormat = "CategoryEdit?categoryId=%s")

    object ItemsList : Screen(routeDefinition = "ItemsList")
    object ItemCreate : Screen(routeDefinition = "ItemCreate?prefillName={prefillName}",
            routeFormat = "ItemCreate?prefillName=%s", routeDefault = "ItemCreate")

    object ItemEdit : Screen(routeDefinition = "ItemEdit?itemId={itemId}",
            routeFormat = "ItemEdit?itemId=%s")

    object StoreEdit : Screen(routeDefinition = "StoreEdit")
    object StoresList : Screen(routeDefinition = "StoresList")
}


@ExperimentalFoundationApi
@Composable
fun BasicNav(startScreen: Screen) {
    val navController = rememberNavController()
    NavHost(navController, startDestination = startScreen.routeDefinition) {
        composable(Screen.IntroGreeting.routeDefinition) {
            IntroGreetingScreen(navController)
        }
        composable(Screen.IntroDone.routeDefinition) {
            IntroDoneScreen(navController)
        }

        composable(Screen.List.routeDefinition) {
            ListScreen(navController)
        }
        composable(Screen.AddToList.routeDefinition) {
            AddToListScreen(navController)
        }

        composable(Screen.Shopping.routeDefinition) {
            ShoppingScreen()
        }

        composable(Screen.CategoriesList.routeDefinition) {
            CategoriesListScreen(navController)
        }
        composable(Screen.CategoryCreate.routeDefinition) {
            CategoryEditScreen(null, navController)
        }
        composable(Screen.CategoryEdit.routeDefinition,
                arguments = listOf(navArgument("categoryId") { })
        ) { backStackEntry ->
            CategoryEditScreen(backStackEntry.arguments?.getString("categoryId"), navController)
        }

        composable(Screen.ItemsList.routeDefinition) {
            ItemsListScreen(navController)
        }
        composable(Screen.ItemEdit.routeDefinition,
                arguments = listOf(navArgument("itemId") { })
        ) { backStackEntry ->
            ItemEditScreen(backStackEntry.arguments?.getString("itemId"), null, navController)
        }
        composable(Screen.ItemCreate.routeDefinition,
                arguments = listOf(navArgument("prefillName") { nullable = true })
        ) { backStackEntry ->
            ItemEditScreen(null, backStackEntry.arguments?.getString("prefillName"), navController)
        }

        composable(Screen.StoreEdit.routeDefinition) {
            StoreEditScreen()
        }
        composable(Screen.StoresList.routeDefinition) {
            StoresListScreen()
        }
    }
}


// Helper Functions:

fun NavController.navigate(
        destination: Screen,
        vararg args: String
) {
    val finalRoute: String = when {
        args.isNotEmpty() -> destination.routeFormat?.format(*args)
                ?: throw IllegalArgumentException(
                        "Args passed but no route with args set for given Screen."
                )
        else -> destination.routeDefault
                ?: destination.routeDefinition
    }

    this.navigate(finalRoute)
}
