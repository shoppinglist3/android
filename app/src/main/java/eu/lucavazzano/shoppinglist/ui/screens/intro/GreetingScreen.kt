package eu.lucavazzano.shoppinglist.ui.screens.intro

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.edgePadding
import eu.lucavazzano.shoppinglist.ui.util.design.imgEdgePadding
import eu.lucavazzano.shoppinglist.ui.util.design.imgHeight
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@Composable
fun IntroGreetingScreen(navController: NavHostController?) {
    Scaffold(
        bottomBar = {
            IntroButtonBar(
                currentIntroStep = IntroStep.GREETING,
                navController = navController,
                showBack = false,
            )
        }
    ) {
        Content()
    }
}

@Composable
private fun Content() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        RoboImage(Modifier.imgEdgePadding())
        Text(
                text = getString(R.string.title_welcome),
                style = typography.h4,
        )
        Text(
                text = getString(R.string.label_tutorial_intro),
                style = typography.h6,
                textAlign = TextAlign.Center,
                modifier = Modifier.edgePadding()
        )
    }
}

@Composable
private fun RoboImage(modifier: Modifier = Modifier) {
    val image: Painter = painterResource(id = R.drawable.ic_shop_robot)
    Image(
            painter = image,
            contentDescription = null, // purely decorative
            modifier = modifier.imgHeight()
    )
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        IntroGreetingScreen(null)
    }
}
