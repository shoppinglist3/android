package eu.lucavazzano.shoppinglist.ui.util.helpers

import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext

@Composable
fun getString(@StringRes resId: Int): String {
    return LocalContext.current.getString(resId)
}

@Composable
fun getString(@StringRes resId: Int, vararg args: String): String {
    return LocalContext.current.getString(resId, *args)
}
