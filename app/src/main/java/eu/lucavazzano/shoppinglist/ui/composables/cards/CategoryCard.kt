package eu.lucavazzano.shoppinglist.ui.composables.cards

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.data.resolveIconName
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.contentPaddingSize
import eu.lucavazzano.shoppinglist.ui.util.design.typography
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@Composable
fun CategoryCard(
        name: String,
        color: Color,
        iconName: IconName,
        onClick: () -> Unit,
        modifier: Modifier = Modifier,
        showEditIcon: Boolean = true,
) {
    Card(
            backgroundColor = color.copy(alpha = 0.7f),
            modifier = modifier
                    .fillMaxWidth()
                    .clickable(onClick = onClick)
    ) {
        ConstraintLayout(
            modifier = Modifier.contentPadding()
        ) {
            val (leadingIcon, label, trailingIcon) = createRefs()
            val margin = contentPaddingSize

            Icon(
                    imageVector = resolveIconName(iconName),
                    contentDescription = null, // purely decorative
                    modifier = Modifier.constrainAs(leadingIcon) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                    }
            )
            Text(
                    text = name,
                    style = typography.h6,
                    modifier = Modifier.constrainAs(label) {
                        top.linkTo(parent.top, margin = margin)
                        bottom.linkTo(parent.bottom, margin = margin)
                        linkTo(
                                start = leadingIcon.end,
                                end = trailingIcon.start,
                                startMargin = margin,
                                endMargin = margin,
                                bias = 0.0f
                        )
                    }
            )
            when (showEditIcon) {
                true -> Icon(
                        imageVector = Icons.Filled.Edit,
                        contentDescription = getString(R.string.title_category_edit),
                        modifier = Modifier
                                .constrainAs(trailingIcon) {
                                    top.linkTo(parent.top)
                                    bottom.linkTo(parent.bottom)
                                    end.linkTo(parent.end, margin = margin)
                                }
                                .alpha(0.7f)
                )
                else -> Box(
                        modifier = Modifier.constrainAs(trailingIcon) {
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                            end.linkTo(parent.end)
                        }
                ) { }
            }
        }
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        CategoryCard("Frozen Food", listVividColors()[7], IconName.Frozen, {})
    }
}

@Preview
@Composable
private fun LongTextPreview() {
    ShoppingListTheme(darkTheme = true) {
        CategoryCard(
                "Another very special category which has a name that is going on for way too long",
                listVividColors()[3], IconName.Android, {}
        )
    }
}

@Preview
@Composable
private fun NoEditPreview() {
    ShoppingListTheme(darkTheme = true) {
        CategoryCard("Snacks", listVividColors()[1], IconName.Frozen, {}, showEditIcon = false)
    }
}
