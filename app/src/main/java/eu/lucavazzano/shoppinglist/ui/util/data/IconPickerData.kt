package eu.lucavazzano.shoppinglist.ui.util.data

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.ui.graphics.vector.ImageVector

enum class IconName {
    Frozen,
    Tea,
    Wine,
    Art,
    Android,
    Book,
}

fun listAllIcons(): Array<IconName> {
    return IconName.values()
}

fun resolveIconName(name: IconName): ImageVector {
    return when (name) {
        IconName.Frozen -> Icons.Outlined.AcUnit
        IconName.Tea -> Icons.Outlined.EmojiFoodBeverage
        IconName.Wine -> Icons.Outlined.WineBar
        IconName.Art -> Icons.Outlined.Brush
        IconName.Android -> Icons.Outlined.Adb
        IconName.Book -> Icons.Outlined.Class
    }
}
