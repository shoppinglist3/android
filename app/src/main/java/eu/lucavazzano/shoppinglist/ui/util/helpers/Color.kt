package eu.lucavazzano.shoppinglist.ui.util.helpers

import androidx.compose.runtime.saveable.Saver
import android.graphics.Color as GraphicsColor
import androidx.compose.ui.graphics.Color as ComposeColor

@Suppress("EXPERIMENTAL_API_USAGE")
val ColorSaver = Saver<ComposeColor, Long>(
    save = { it.value.toLong() },
    restore = { ComposeColor(it) }
)

fun GraphicsColor.toCompose(): ComposeColor {
    return ComposeColor(this.red(), this.green(), this.blue(), this.alpha())
}

fun ComposeColor.toGraphics(): GraphicsColor {
    return GraphicsColor.valueOf(this.red, this.green, this.blue, this.alpha)
}
