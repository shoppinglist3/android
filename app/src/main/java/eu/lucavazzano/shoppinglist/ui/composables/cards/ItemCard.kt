package eu.lucavazzano.shoppinglist.ui.composables.cards

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Close
import androidx.compose.material.icons.sharp.Edit
import androidx.compose.material.icons.sharp.PlusOne
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.resolveIconName
import eu.lucavazzano.shoppinglist.ui.util.design.*
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@Composable
fun ItemCard(
    ribbonColor: Color,
    iconName: IconName,
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    trailingIcon: ImageVector = Icons.Sharp.Edit,
) {
    Card(
        elevation = 4.dp,
        modifier = modifier
            .fillMaxWidth()
            .clickable(onClick = onClick)
    ) {
        ConstraintLayout {
            val (ribbon, label, trailingBtn) = createRefs()
            val margin = contentPaddingSize

            Surface(
                color = ribbonColor.copy(alpha = 0.7f),
                modifier = Modifier.constrainAs(ribbon) {
                    start.linkTo(parent.start)
                    linkTo(
                        top = parent.top,
                        bottom = parent.bottom
                    )
                    height = Dimension.fillToConstraints
                }
            ) {
                Icon(
                    imageVector = resolveIconName(iconName),
                    contentDescription = null, // purely decorative
                    modifier = Modifier.contentPadding()
                )
            }
            Text(
                text = text,
                style = typography.h6,
                modifier = Modifier.constrainAs(label) {
                    top.linkTo(parent.top, margin = margin)
                    bottom.linkTo(parent.bottom, margin = margin)
                    linkTo(
                        start = ribbon.end,
                        end = trailingBtn.start,
                        startMargin = margin,
                        endMargin = margin,
                        bias = 0.0f
                    )
                    width = Dimension.preferredWrapContent
                }
            )
            Icon(
                imageVector = trailingIcon,
                contentDescription = getString(R.string.title_item_edit),
                modifier = Modifier
                    .constrainAs(trailingBtn) {
                        top.linkTo(parent.top)
                        bottom.linkTo(parent.bottom)
                        end.linkTo(parent.end, margin = margin)
                    }
                    .alpha(0.5f)
            )
        }
    }
}

@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        ItemCard(
            Color(0xFF008040),
            IconName.Android,
            "Cherry Tomatoes",
            {},
            Modifier.edgePadding()
        )
    }
}

@Preview
@Composable
private fun LightPreview() {
    ShoppingListTheme {
        ItemCard(
            Color(0xFF008040),
            IconName.Android,
            "Cherry Tomatoes",
            {},
            Modifier.edgePadding()
        )
    }
}

@Preview
@Composable
private fun LongTextPreview() {
    ShoppingListTheme(darkTheme = true) {
        ItemCard(
            Color(0xFFF4C424),
            IconName.Frozen,
            "Some very good cheese that's strong in taste while nice and easy to grate",
            {},
            Modifier.edgePadding()
        )
    }
}

@Preview
@Composable
private fun TrailingIconPreview() {
    ShoppingListTheme(darkTheme = true) {
        ItemCard(
            Color(0xFF008040),
            IconName.Android,
            "Limes",
            {},
            Modifier.edgePadding(),
            trailingIcon = Icons.Sharp.Close
        )
    }
}
