package eu.lucavazzano.shoppinglist.ui.screens.settings

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.modelViews.ShopItemExpanded
import eu.lucavazzano.shoppinglist.repository.AppDatabase
import eu.lucavazzano.shoppinglist.repository.shopItem.ShopItemRepository
import eu.lucavazzano.shoppinglist.ui.composables.cards.ItemCard
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.SectionSpacer
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.edgePadding
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import eu.lucavazzano.shoppinglist.ui.util.helpers.toGraphics
import eu.lucavazzano.shoppinglist.ui.util.navigation.Screen
import eu.lucavazzano.shoppinglist.ui.util.navigation.navigate

class ItemsListScreenViewModel(
        context: Context
) {
    var items: LiveData<List<ShopItemExpanded>>

    init {
        val dao = AppDatabase.getDatabase(context).getShopItemDao()
        val repository = ShopItemRepository(dao)
        items = repository.items
    }
}


@Composable
fun ItemsListScreen(navHostController: NavController) {
    ItemsListScreen(
            vm = ItemsListScreenViewModel(LocalContext.current),
            navHostController = navHostController
    )
}

@Composable
fun ItemsListScreen(vm: ItemsListScreenViewModel, navHostController: NavController) {
    val itemsList by vm.items.observeAsState(initial = emptyList())

    ItemsListScreen(
            items = itemsList,
            onEditClick = { itemId ->
                navHostController.navigate(Screen.ItemEdit, itemId.toString())
            },
            onNewClick = {
                navHostController.navigate(Screen.ItemCreate)
            }
    )
}

@Composable
fun ItemsListScreen(
        items: List<ShopItemExpanded>,
        onEditClick: (Long) -> Unit,
        onNewClick: () -> Unit,
) {
    Scaffold(
            content = { innerPadding ->
                Column(
                        modifier = Modifier
                                .padding(innerPadding)
                                .edgePadding()
                ) {
                    Text(
                            text = getString(R.string.label_list_item),
                            style = MaterialTheme.typography.h4,
                    )
                    SectionSpacer()
                    LazyColumn(
                            modifier = Modifier.fillMaxWidth()
                    ) {
                        items(items = items) {
                            ItemCard(
                                    ribbonColor = it.categoryColor.toCompose(),
                                    iconName = it.categoryIconName,
                                    text = it.name,
                                    onClick = { onEditClick(it.id) },
                                    modifier = Modifier.contentPadding()
                            )
                        }
                    }
                }
            },
            floatingActionButton = {
                FloatingActionButton(
                        contentColor = MaterialTheme.colors.onPrimary,
                        onClick = onNewClick
                ) {
                    Icon(
                            imageVector = Icons.Filled.Add,
                            contentDescription = getString(R.string.describe_add_new)
                    )
                }
            }
    )
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        ItemsListScreen(
                items = listOf(
                        ShopItemExpanded(
                                0, "Mini Carrots",
                                0, "Vegetables", listVividColors()[16].toGraphics(), IconName.Art
                        ),
                        ShopItemExpanded(
                                0, "High Quality Rum",
                                0, "Drinks", listVividColors()[8].toGraphics(), IconName.Android
                        ),
                        ShopItemExpanded(
                                0, "Frozen Asia Vegetables",
                                0, "Frozen Food", listVividColors()[5].toGraphics(), IconName.Frozen
                        )
                ),
                onEditClick = { },
                onNewClick = { }
        )
    }
}
