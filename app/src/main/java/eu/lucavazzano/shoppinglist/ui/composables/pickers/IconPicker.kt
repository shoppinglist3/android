package eu.lucavazzano.shoppinglist.ui.composables.pickers

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.ui.util.data.IconName
import eu.lucavazzano.shoppinglist.ui.util.data.listAllIcons
import eu.lucavazzano.shoppinglist.ui.util.data.resolveIconName
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.contentPaddingSize
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString

@ExperimentalFoundationApi
@Composable
fun IconPicker(onSelect: (v: IconName) -> Unit, modifier: Modifier = Modifier) {
    val iconSize = 32.dp
    val cellSize = iconSize + contentPaddingSize

    Picker(
            title = getString(R.string.label_select_icon),
            minSize = cellSize,
            modifier = modifier
    ) {
        items(items = listAllIcons()) {
            IconButton(
                    onClick = { onSelect(it) },
                    modifier = Modifier.contentPadding()
            ) {
                Icon(
                        imageVector = resolveIconName(it),
                        contentDescription = getString(R.string.describe_icon),
                        modifier = Modifier
                                .requiredSize(iconSize)
                                .alpha(0.7f)
                )
            }
        }
    }
}


@ExperimentalFoundationApi
@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        IconPicker({})
    }
}
