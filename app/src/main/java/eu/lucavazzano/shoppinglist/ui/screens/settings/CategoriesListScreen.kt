package eu.lucavazzano.shoppinglist.ui.screens.settings

import android.content.Context
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.LiveData
import androidx.navigation.NavHostController
import eu.lucavazzano.shoppinglist.R
import eu.lucavazzano.shoppinglist.models.ShopCategory
import eu.lucavazzano.shoppinglist.repository.AppDatabase
import eu.lucavazzano.shoppinglist.repository.shopcategory.ShopCategoryRepository
import eu.lucavazzano.shoppinglist.ui.composables.cards.CategoryCard
import eu.lucavazzano.shoppinglist.ui.util.data.listAllIcons
import eu.lucavazzano.shoppinglist.ui.util.data.listVividColors
import eu.lucavazzano.shoppinglist.ui.util.design.SectionSpacer
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.design.contentPadding
import eu.lucavazzano.shoppinglist.ui.util.design.edgePadding
import eu.lucavazzano.shoppinglist.ui.util.helpers.getString
import eu.lucavazzano.shoppinglist.ui.util.helpers.toCompose
import eu.lucavazzano.shoppinglist.ui.util.helpers.toGraphics
import eu.lucavazzano.shoppinglist.ui.util.navigation.Screen
import eu.lucavazzano.shoppinglist.ui.util.navigation.navigate

class CategoriesListViewModel(
    context: Context
) {
    var categories: LiveData<List<ShopCategory>>

    init {
        val dao = AppDatabase.getDatabase(context).getShopCategoryDao()
        val repository = ShopCategoryRepository(dao)
        categories = repository.items
    }
}


@Composable
fun CategoriesListScreen(navHostController: NavHostController) {
    CategoriesListScreen(CategoriesListViewModel(LocalContext.current), navHostController)
}

@Composable
fun CategoriesListScreen(vm: CategoriesListViewModel, navHostController: NavHostController) {
    val categoriesList by vm.categories.observeAsState(initial = emptyList())

    CategoriesListScreen(
            categoriesList = categoriesList,
            onEditClick = {
                navHostController.navigate(Screen.CategoryEdit, it.id.toString())
            },
            onNewClick = {
                navHostController.navigate(Screen.CategoryCreate)
            }
    )
}

@Composable
fun CategoriesListScreen(
    categoriesList: List<ShopCategory>,
    onEditClick: (ShopCategory) -> Unit,
    onNewClick: () -> Unit,
) {
    Scaffold(
        floatingActionButton = {
            FloatingActionButton(
                contentColor = MaterialTheme.colors.onPrimary,
                onClick = { onNewClick() },
            ) {
                Icon(
                        imageVector = Icons.Filled.Add,
                        contentDescription = getString(R.string.describe_add_new)
                )
            }
        }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                    .padding(innerPadding)
                    .edgePadding()
        ) {
            Text(
                    text = getString(R.string.label_list_category),
                    style = MaterialTheme.typography.h4,
            )
            SectionSpacer()
            LazyColumn(
                    modifier = Modifier.fillMaxWidth()
            ) {
                items(items = categoriesList) {
                    CategoryCard(
                            it.name, it.color.toCompose(), it.iconName,
                            onClick = { onEditClick(it) },
                            modifier = Modifier.contentPadding()
                    )
                }
            }
        }
    }
}


@Preview
@Composable
private fun DefaultPreview() {
    ShoppingListTheme(darkTheme = true) {
        CategoriesListScreen(
            categoriesList = listOf(
                ShopCategory(
                    "Vegetables",
                    listVividColors().random().toGraphics(), listAllIcons().random()
                ),
                ShopCategory(
                    "Cool Culinary Ingredients",
                    listVividColors().random().toGraphics(), listAllIcons().random()
                ),
                ShopCategory(
                    "One more random Category with a name that is way too long",
                    listVividColors().random().toGraphics(), listAllIcons().random()
                )
            ),
            onNewClick = {},
            onEditClick = {},
        )
    }
}
