package eu.lucavazzano.shoppinglist.ui.util.data

import androidx.compose.ui.graphics.Color

fun listVividColors(): Array<Color> {
    return arrayOf(
        // https://www.material.io/design/color/the-color-system.html#tools-for-picking-colors
        // 2014 color palette, 800 saturation
        Color(0xFFC62828),
        Color(0xFFAD1457),
        Color(0xFF6A1B9A),
        Color(0xFF4527A0),
        Color(0xFF283593),
        Color(0xFF1565C0),
        Color(0xFF0277BD),
        Color(0xFF00838F),
        Color(0xFF00695C),
        Color(0xFF2E7D32),
        Color(0xFF008040),
        Color(0xFF558B2F),
        Color(0xFF9E9D24),
        Color(0xFFF9A825),
        Color(0xFFFF8F00),
        Color(0xFFEF6C00),
        Color(0xFFD84315),
        Color(0xFF4E342E),
        Color(0xFF37474F),
    )
}
