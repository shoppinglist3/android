package eu.lucavazzano.shoppinglist

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ExperimentalFoundationApi
import eu.lucavazzano.shoppinglist.ui.screens.intro.getStartScreenForSavedIntroStep
import eu.lucavazzano.shoppinglist.ui.util.design.ShoppingListTheme
import eu.lucavazzano.shoppinglist.ui.util.navigation.BasicNav

class MainActivity : AppCompatActivity() {
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val startScreen = getStartScreenForSavedIntroStep(baseContext)

        setContent {
            ShoppingListTheme {
                BasicNav(startScreen)
            }
        }
    }

}
