package eu.lucavazzano.shoppinglist.models

import androidx.room.Entity
import androidx.room.PrimaryKey

const val LIST_ITEM_TABLE_NAME = "list_item_table"

@Entity(tableName = LIST_ITEM_TABLE_NAME)
data class ListItem(
        val amount: String,
        val note: String,
        val shopItemId: Long,
        @PrimaryKey(autoGenerate = true) val id: Long = 0
)
