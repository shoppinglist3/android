package eu.lucavazzano.shoppinglist.models

import androidx.room.Entity
import androidx.room.PrimaryKey

const val SHOP_ITEM_TABLE_NAME = "shop_item_table"

@Entity(tableName = SHOP_ITEM_TABLE_NAME)
data class ShopItem(
        val name: String,
        val categoryId: Long,
        @PrimaryKey(autoGenerate = true) val id: Long = 0, // 0 = not set, replace on DB insertion
)
