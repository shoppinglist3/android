package eu.lucavazzano.shoppinglist.models

import android.graphics.Color
import androidx.room.Entity
import androidx.room.PrimaryKey
import eu.lucavazzano.shoppinglist.ui.util.data.IconName

const val SHOP_CATEGORY_TABLE_NAME = "shop_category_table"

@Entity(tableName = SHOP_CATEGORY_TABLE_NAME)
data class ShopCategory(
        val name: String,
        val color: Color,
        val iconName: IconName,
        @PrimaryKey(autoGenerate = true) val id: Long = 0, // 0 = not set, replace on DB insertion
)
