package eu.lucavazzano.shoppinglist

import eu.lucavazzano.shoppinglist.ui.screens.main.splitInput
import org.junit.Assert.assertEquals
import org.junit.Test

class AddToListScreenTest {
    @Test
    fun splitInput_isCorrect() {
        assertEquals(Pair("", ""), splitInput(""))

        assertEquals(Pair("", "Apples"), splitInput("Apples"))
        assertEquals(Pair("10", "Apples"), splitInput("10 Apples"))

        assertEquals(Pair("100ml", "Cream"), splitInput("100ml Cream"))
        assertEquals(Pair("100Ml", "Cream"), splitInput("100Ml Cream"))
        assertEquals(Pair("100mL", "Cream"), splitInput("100mL Cream"))
        assertEquals(Pair("100ML", "Cream"), splitInput("100ML Cream"))

        assertEquals(Pair("10 kg", "Cheese"), splitInput("10 kg Cheese"))
        assertEquals(Pair(".1 kg", "Cheese"), splitInput(".1 kg Cheese"))
        assertEquals(Pair("1 Pkg", "Baking Powder"), splitInput("1 Pkg Baking Powder"))

        assertEquals(Pair("10", "Limes"), splitInput("10  Limes"))
        assertEquals(Pair("10", "Limes"), splitInput("10               Limes"))
    }
}
