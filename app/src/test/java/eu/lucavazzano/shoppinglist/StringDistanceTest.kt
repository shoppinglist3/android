package eu.lucavazzano.shoppinglist

import eu.lucavazzano.shoppinglist.ui.util.helpers.computeStringDistance
import org.junit.Assert.assertEquals
import org.junit.Test

@Suppress("SpellCheckingInspection")
class StringDistanceTest {
    @Test
    fun empty_isCorrect() {
        assertEquals(0.0f, computeStringDistance("", ""))
    }

    @Test
    fun equal_isCorrect() {
        assertEquals(0.0f, computeStringDistance("abc", "abc"))
    }

    @Test
    fun substitution_isCorrect() {
        assertEquals(1.0f, computeStringDistance("a", "b"))
        assertEquals(1.0f, computeStringDistance("xax", "xbx"))

        assertEquals(10.0f, computeStringDistance(
                "xax", "xbx",
                deleteCost = Float.MAX_VALUE,
                insertCost = Float.MAX_VALUE,
                substituteCost = 10.0f,
                transposeCost = Float.MAX_VALUE
        ))
    }

    @Test
    fun deletion_isCorrect() {
        assertEquals(1.0f, computeStringDistance("a", ""))
        assertEquals(1.0f, computeStringDistance("xabc", "abc"))
        assertEquals(1.0f, computeStringDistance("axbc", "abc"))
        assertEquals(1.0f, computeStringDistance("abxc", "abc"))
        assertEquals(1.0f, computeStringDistance("abcx", "abc"))

        assertEquals(10.0f, computeStringDistance(
                "abcx", "abc",
                deleteCost = 10.0f,
                insertCost = Float.MAX_VALUE,
                substituteCost = Float.MAX_VALUE,
                transposeCost = Float.MAX_VALUE
        ))
    }

    @Test
    fun insertion_isCorrect() {
        assertEquals(1.0f, computeStringDistance("", "a"))
        assertEquals(1.0f, computeStringDistance("abc", "xabc"))
        assertEquals(1.0f, computeStringDistance("abc", "axbc"))
        assertEquals(1.0f, computeStringDistance("abc", "abxc"))
        assertEquals(1.0f, computeStringDistance("abc", "abcx"))

        assertEquals(10.0f, computeStringDistance(
                "abc", "abcx",
                deleteCost = Float.MAX_VALUE,
                insertCost = 10.0f,
                substituteCost = Float.MAX_VALUE,
                transposeCost = Float.MAX_VALUE
        ))
    }

    @Test
    fun transposition_isCorrect() {
        assertEquals(1.0f, computeStringDistance("teh", "the"))
        assertEquals(1.0f, computeStringDistance("tets", "test"))
        assertEquals(1.0f, computeStringDistance("fuor", "four"))

        assertEquals(10.0f, computeStringDistance(
                "tets", "test",
                deleteCost = Float.MAX_VALUE,
                insertCost = Float.MAX_VALUE,
                substituteCost = Float.MAX_VALUE,
                transposeCost = 10.0f
        ))
    }

    @Test
    fun multiple_isCorrect() {
        assertEquals(3.0f, computeStringDistance("kitten", "sitting"))
        assertEquals(3.0f, computeStringDistance("Saturday", "Sunday"))
        assertEquals(8.0f, computeStringDistance("rosettacode", "raisethysword"))
    }
}
